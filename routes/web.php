<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('sendmessage', 'chatController@sendMessage');

Route::get('fire', function () {
    // this fires the event
    event(new App\Events\EventName());
    return "event fired";
})->middleware('auth');

Route::get('test', function () {
    // this checks for the event
    return view('test');
})->middleware('auth');