<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use LRedis;
use App\User;

class chatController extends Controller
{
    public function index()
    {
        $users = User::all();

        return view('chat',compact('users'));
    }

	public function sendMessage(Request $request)
    {
		$redis = LRedis::connection();
		$data = ['message' => $request->message, 'from' => $request->from,'to' => $request->to];
		$redis->publish('message', json_encode($data));

		return response()->json($data);
	}	
}
